function adminLogin(){			// Login

	var username = $('#adminLoginUsernameInput').val();
	var password = $('#adminLoginPasswordInput').val();

	var html = $.ajax({
		type: "POST",
		url: smsAPI,
		data: "action=smsLogin&username="+username+"&password="+password,
		async: false
	}).responseText;

	var results = JSON.parse(html);
	if(results['returned'] == 0){
		$.cookie('uId', results.admin.id, { expires: 30 });
		$.cookie('hash', results.admin.hash, { expires: 30 });
		$.cookie('name', results.admin.name, { expires: 30 });
		$.cookie('bId', results.admin.bId, { expires: 30 });
		location.reload();
		//$('#loginOverlay').fadeIn(500,function(){ adminStatsOverview() });
	}else if(results['returned'] == 1){
		alert('This user does not exist') 
	}else if(results['returned'] == 2){
		alert('This user is not an admin') 
	}else if(results['returned'] == 3){
		alert('The password is incorrect') 
	}else if(results['returned'] == 4){
		alert('This user is banned') 	
	}else
		alert(html)	
}


function smsChangePassword(){

	var uId = $.cookie('uId');
	var hash = $.cookie('hash');
	var oldPass = $('#adminChangePassOld').val();
	var newPass = $('#adminChangePassNew').val();
	var newPass2 = $('#adminChangePassNew2').val();

	if(newPass != newPass2){
		notify('error',"Your password didn't match");
		return false;
	}
	
	var html = $.ajax({
		type: "POST",
		url: smsAPI,
		data: "action=smsChangePassword&uId="+uId+"&hash="+hash+"&password="+oldPass+"&newPass="+newPass,
		async: false
	}).responseText;
	var results = JSON.parse(html);
	
	if(results.returned == 0){
		notify('success','Your password was changed successfully.');	
	}else if(results.returned == 1){
		notify('error','You have been logged out, refresh this page');		
	}else if(results.returned == 2){
		notify('error','The old password is incorrect');	
	}else if(results.returned == 3){
		notify('error','Your password needs to be longer');
	}else
		notify('error','There was a problem posting...');
	
}

function smsContacts(search){

	var uId = $.cookie('uId');
	var hash = $.cookie('hash');
	var bId = $.cookie('bId');

	var html = $.ajax({
		type: "POST",
		url: smsAPI,
		data: "action=smsContacts&uId="+uId+"&hash="+hash+"&bId="+bId+"&search="+search,
		async: false
	}).responseText;
	var results = JSON.parse(html);
	
	if(results.returned == 0){
		var contactList = '';
		$.each(results.contacts, function(i,v) {
			
			contactList += "<div class='row contactRow' style='' id='"+v.id+"'>";
				contactList += "<div class='columns large-5 medium-5 small-5' style='text-align:left;'><i class='fa fa-user'></i> "+v.lastName+", "+v.firstName+"</div>";
				contactList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-phone'></i> "+v.phone+"</div>";
				contactList += "<div class='columns large-2 medium-2 small-2'> </div>";
			contactList += "</div><hr>";
				
		});	
		$('#contactList').html(contactList);
		
	}else if(results.returned == 1)
		notify('error','Your login credentials are invalid');		
	else if(results.returned == 2)
		notify('error','No business with this ID exists');	
	else if(results.returned == 3)
		notify('error','That account already exists in Prod');
	
}

function smsHistory(search){

	var uId = $.cookie('uId');
	var hash = $.cookie('hash');
	var bId = $.cookie('bId');

	var html = $.ajax({
		type: "POST",
		url: smsAPI,
		data: "action=smsHistory&uId="+uId+"&hash="+hash+"&bId="+bId+"&search="+search,
		async: false
	}).responseText;
	var results = JSON.parse(html);
	
	if(results.returned == 0){
		var historyList = '';var scheduledList = '';
		if(results.history){
			$.each(results.history, function(i,v) {
				
				historyList += "<div class='row historyRow' style='' id='"+v.id+"'>";
					if(v.lastName != '')
						historyList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-user'></i> "+v.lastName+", "+v.firstName+"</div>";
					else
						historyList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-phone'></i> "+v.phone+"</div>";
					historyList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-calendar-o'></i> "+v.dateSent+"</div>";
				historyList += "</div>";
				historyList += "<div class='row'><div class='columns large-12 medium-12 small-12'><i class='fa fa-comment-o'></i> "+v.message+"</div></div><hr>";
					
			});	
			$('#sentHistory').html(historyList);
		}
		
		if(results.scheduled){
			$.each(results.scheduled, function(i,v) {
				
				scheduledList += "<div class='row scheduledRow' style='' id='"+v.id+"'>";
					if(v.lastName != '')
						scheduledList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-user'></i> "+v.lastName+", "+v.firstName+"</div>";
					else
						scheduledList += "<div class='columns large-5 medium-5 small-5'><i class='fa fa-phone'></i> "+v.phone+"</div>";
					scheduledList += "<div class='columns large-3 medium-3 small-3'><i class='fa fa-calendar-o'></i> "+v.dateSent+"</div>";
					scheduledList += "<div class='columns large-3 medium-3 small-3'><i class='fa fa-clock-o'></i> "+v.dateScheduled+"</div>";
				scheduledList += "</div>";
				scheduledList += "<div class='row'><div class='columns large-12 medium-12 small-12'><i class='fa fa-comment-o'></i> "+v.message+"</div></div><hr>";
					
			});			
			$('#scheduledHistory').html(scheduledList);
		}
	}else if(results.returned == 1)
		notify('error','Your login credentials are invalid');		
	else if(results.returned == 2)
		notify('error','No business with this ID exists');	
	else if(results.returned == 3)
		notify('error','That account already exists in Prod');
	
}

function smsSend(scheduled){

	var uId = $.cookie('uId');
	var hash = $.cookie('hash');
	var bId = $.cookie('bId');
	var smsTo = $('#composePhone input').val();
	var smsBody = $('#composeText').val();
	var smsFirst = $('#composeFirst input').val();
	var smsLast = $('#composeLast input').val();
	var smsTags = $('#composeTags input').val();
	if(scheduled == 1)
		var smsScheduled = $('#composeScheduled').val();
	else
		smsScheduled = '';
	
	if(smsTo == '' || smsBody == ''){
		notify('error','Make sure to put in a phone number and message to send.');
		return false;
	}
	
	var html = $.ajax({
		type: "POST",
		url: smsAPI,
		data: "action=smsSend&uId="+uId+"&hash="+hash+"&bId="+bId+"&smsTo="+smsTo+"&smsBody="+smsBody+"&smsFirst="+smsFirst+"&smsLast="+smsLast+"&smsTags="+smsTags+"&smsScheduled="+smsScheduled,
		async: false
	}).responseText;
	var results = JSON.parse(html);
	
	if(results.returned == 0){
		if(scheduled == 0)
			notify('success','Your message has been sent successfully!');
		else
			notify('success','Your message has been scheduled to be sent!');

		$('#composePhone input').val('');		
		$('#composeText').val('');		
		$('#composeFirst input').val('');
		$('#composeLast input').val('');	
		$('#composeTags input').val('');	
		$('#composeScheduled').val('');	
		smsHistory('');
		smsContacts('');
			
	}else if(results.returned == 3)		// payment issues
		notify('error','Your account has been disable due to payment. Please contact sales@busyseed.com for more info.');	
	else if(results.returned == 4)		// disabled due to abuse
		notify('error','Your account has been flagged for abuse and has been disabled. Please contact support@busyseed.com for more info.');	
	else if(results.returned == 5)		// business DNE
		notify('error','Business DNE. Please contact support@busyseed.com.');	
	else if(results.returned == 6)     	// exceeded SMS limit
		notify('error','You have exceeded your text message allotment. Please email sales@busyseed.com to increase.');	
	else if(results.returned == 7)     	// SMS user is disabled
		notify('error','This contact has been blocked from sending messages.');	
	else if(results.returned == 8)     	// SMS user has opt-out
		notify('error','This user has opted out of receiving messages.');	
	else if(results.returned == 9)     	// Twilio problem
		notify('error',results.message);	
}

function showLogin(){
	// Show the login
	$('#loginOverlay').fadeIn(500);
}

function notify(type,text){
	var n = noty({
		layout: 'bottom',
		type: type,
		theme: 'defaultTheme', // or 'relax'
		text: text,
		timeout: 5000, // delay for closing event. Set false for sticky notifications
		animation: {
			open: {height: 'toggle'}, // jQuery animate function property object
			close: {height: 'toggle'}, // jQuery animate function property object
			easing: 'swing', // easing
			speed: 500 // opening & closing animation speed
		}
	});
}

$(document).ready(function(){
	
	// Load info
	$('.staffFirstName').html($.cookie('name'));
	$('.staffPic').attr('src',$.cookie('pic'));
	
	$("#loginBox").keypress(function(event) {
		if (event.which == 13) { 
			event.preventDefault();
			$('#adminLoginSubmit').trigger('click');
		}
	});
	$('#adminLoginSubmit').click(function(){ 
		$(this).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
		adminLogin(); 
	});

	
	// User settings change
	$('#staffAccount').click(function(){  
		$('#userOverlay').fadeIn(300).click(function(e){
			if(e.target.id == $(this).attr('id'))
				$('#userOverlay').fadeOut(300);
		});
	});
	$('#adminChangePassSubmit').click(function(){ smsChangePassword(); });
	
});