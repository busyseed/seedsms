<?php
include 'header.php';
	
// Check if they are logged in (mask it in server side)
if(!isset($_COOKIE['uId']) || !isset($_COOKIE['hash']))
	$loggedIn = 0;
else
	$loggedIn = 1;
?>

<script src="js/chart.min.js"></script>	
<script>
$(document).ready(function(){
	if(<?=$loggedIn?> == 0)
		showLogin();
});
</script>

<script>
$(document).ready(function(){

	layoutHTML = $('#accountOverviewLayout').html(); 						// Save layout for future reference
	
	// Get data and they will automatically display
	smsHistory('');
	smsContacts('');

	// Show contact
	$('#composeShowContact').click(function(){
		$(this).fadeOut(300,function(){
			$('#composeContactCont').fadeIn(300);
		});
	});
	
	// Count characters
    $('#composeText').keyup(function() {
        var textCount = $('#composeText').val().length;
		var textCountMessages = parseInt((Math.floor(textCount/160)).toFixed(0))+1;
        $('#composeTextCount').html(textCount+' characters will be sent in '+textCountMessages+' message(s)');
    });
	
	$('#accountSearchInput').keyup(function(){
		smsContacts($(this).val());
	});
	
	// Show the time scheduler
	$('#smsSchedule').one('click',function(){$('#scheduleButtonText').fadeOut(300,function(){ $('#composeScheduler').fadeIn(300) });});
	$('#composeScheduled').intimidatetime({
		format: 'yyyy-MM-dd HH',
		previewFormat: 'MM/dd/yyyy HH'
	});
	
});
</script>

<div id='accountOverview' class='fullWidth' style='position:relative;height:100%;padding:0rem;overflow-y:scroll;'>

		<div id='accountOverviewLayout row' style='padding-top:5rem;height:100%;'>
		
			<div id='smsContacts' class='columns large-3 medium-3 small-12' style='height:100%;border-right:1px solid #87a65c;padding:1rem;text-align:center;'>
				<div id='contactsTitle' style='font-size:2rem;padding:2rem;color:#87a65c;text-align:center;'><i class='fa fa-search'></i> Find a contact</div>
				<input id='accountSearchInput' type='text' placeholder='Start typing to search for a contact' style='width:80%;display:inline;text-align:center;' />
				<div id='contactList' class=''><div style='text-align:center;text-decoration:italics;'>No contacts have been stored yet.</div></div>
			</div>		
			
			<div id='smsCompose' class='columns large-6 medium-6 small-12' style='height:100%;border-right:1px solid #87a65c;padding:1rem;'>
				<div id='composeTitle' style='font-size:2rem;padding:2rem;color:#87a65c;text-align:center;'><i class='fa fa-paper-plane-o'></i> Send a SMS</div>

				<div id='composePhone' class='columns large-12 medium-12 small-12'><input type='text' style='width:100%;' placeholder='A single phone number or multiple (seperated by commas)' /></div>
				<div class='row'>
					<div id='composeShowContact' class='columns large-12 medium-12 small-12'><input type='checkbox' /> Would you like to save this number as a contact?</div>
				</div>				
				<div id='composeContactCont' class='row' style='display:none;'>
					<div id='composeFirst' class='columns large-4 medium-4 small-12'><input type='text' style='width:100%' placeholder='First name' /></div>
					<div id='composeLast' class='columns large-4 medium-4 small-12'><input type='text' style='width:100%' placeholder='Last name' /></div>
					<div id='composeTags' class='columns large-4 medium-4 small-12'><input type='text' style='width:100%' placeholder='Tags (seperated by commas)' /></div>
				</div>	
				
				<textarea id='composeText' class='columns large-12 medium-12 small-12' placeholder='Enter a message. Messages over 160 characters will be broken up into multiple messages'></textarea>
				<div id='composeTextCount' style='text-align:right;'></div>
				
				<ul class='large-block-grid-2 medium-block-grid-2 small-block-grid-1'>
					<li><button id='smsSend'  onclick="smsSend(0)" style='width:100%;background:#87a65c;margin-top:1rem;'>Send now</button></li>
					<li><button id='smsSchedule' style='width:100%;background:#87a65c;margin-top:1rem;'>
						<span id='scheduleButtonText'>Schedule</span>
						<span id='composeScheduler' style='display:none;'>
							<input type="text" id="composeScheduled" value="" data-intimidatetime="1" placeholder='Pick a date and time'>
							<i  onclick="smsSend(1)" style='font-size:2rem;cursor:pointer;' class='fa fa-check'></i>
						</span>	
					</button></li>
				</ul>
				
				
				<div id='scheduledHistoryTitle' style='font-size:2rem;padding:2rem;color:#87a65c;text-align:center;'><i class='fa fa-clock-o'></i> Scheduled messages</div>
				<div id='scheduledHistory' class=''><div style='text-align:center;text-decoration:italics;'>No scheduled messages are queued</div></div>			
			</div>		
			
			<div id='smsSent' class='columns large-3 medium-3 small-12' style='height:100%;padding:1rem;'>
				<div id='sentHistoryTitle' style='font-size:2rem;padding:2rem;color:#87a65c;text-align:center;'><i class='fa fa-history'></i> Messages Sent</div>
				<div id='sentHistory' class=''><div style='text-align:center;text-decoration:italics;'>No messages have been sent</div></div>		
			</div>
			

	</div> <!-- END accountLayout -->
	
	<!-- Tooltips -->
	<div style='display:none;'>
		<div id='accountOverviewOwnerDetails'>
			<div id='accountOwnerName'>Owner name</div>
			<div id='accountOwnerPhone'>Owner Phone</div>
			<div id='accountOwnerEmail'>Owner Email</div><br>
			<div id='accountOwnerEmailCount'><i class="fa fa-envelope-o"></i> Received: <span></span></div>
			<div id='accountOwnerEmailsOpened'><i class="fa fa-envelope-o"></i> Opened: <span></span></div>
			<div id='accountOwnerEmailsViews'><i class="fa fa-envelope-o"></i> Views: <span></span></div>
			<div id='accountOwnerEmailsClicks'><i class="fa fa-envelope-o"></i> Link Clicks: <span></span></div>
			<div id='accountOwnerEmailsSpam'><i class="fa fa-envelope-o"></i> Sent to Spam: <span></span></div>
			<div id='accountOwnerEmailsAvgOpen'><i class="fa fa-envelope-o"></i> Average Time to Open: <span>0</span> min</div>
		</div>			
	</div>

										
</div>