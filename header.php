<?php
	include '../config.php';
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
   	<meta charset="utf-8" />
   	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
   	<title>BusySeed - SMS Panel</title>
		
   	<link rel="stylesheet" href="/library/foundation/css/foundation.css" />
	<link rel="stylesheet" href="/library/foundation/css/normalize.css" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/library/site/css/animate.css" />
	<link rel="stylesheet" href="/library/site/css/tooltipster.css" />
	<link rel="stylesheet" href="/sms/css/main.css" />

   	<script src="/library/foundation/js/vendor/modernizr.js"></script>
    <script src="/library/foundation/js/vendor/jquery.js"></script>
    <script src="/library/foundation/js/foundation.min.js"></script>
	<script src="/library/site/js/jquery.cookie.js"></script>
	<script src="/library/site/js/matchHeight.min.js" type="text/javascript"></script>
	<script src="/library/site/js/jquery.noty.packaged.min.js"></script>
	<script src="/library/site/js/jquery.transit.min.js"></script>
	<script src="/library/site/js/jquery.tooltipster.min.js"></script>
	<script src="/library/site/js/Intimidatetime.js"></script>

	<script src="/sms/js/main.js"></script>

    <script>
	smsAPI = '<?=API_SMS?>';
	deployState = '<?=DEPLOY_STATE?>';
	$(document).ready(function(){
		$(this).foundation();
	});
    </script>
</head>
<body style='position:absolute;width:100%;max-height:100%;'>

<div id='userOverlay' class='overlay' style='z-index:99;display:none;position:absolute;width:100%;height:100%;background:rgba(255,255,255,.8);'>
	<div id='userInner' class='overlayInner valign' style='width:50%;margin:0 auto;background:#FFF;text-align:center;'>
		<h3>Changing account info</h3>
		<div class='row'>
			<div class='columns large-9 large-centered medium-centered small-centered' style='padding:0 1rem;'>
				<input id='adminChangePassOld' class='expand' type='password' placeholder='Old password' />
				<input id='adminChangePassNew' class='expand' type='password' placeholder='New password' />
				<input id='adminChangePassNew2' class='expand' type='password' placeholder='New password, again' />
				<button id='adminChangePassSubmit' class='expand'>Change your password</button>
			</div>
		</div>
	</div>
</div>

<div id='loginOverlay' style='position:fixed;z-index:10;width:100%;height:100%;background:#FFF;display:none;'>
	<div id='loginBox' class='valign row' style='background:#FFF;height:15rem;width:20rem;border-radius:1rem;'>
		<div id='adminLoginInfo' style='text-align:center;'>Hi there, login to BusySeed SMS</div>
		<div id='adminLoginUsername' class='row'>
			<div class='large-1 columns'><i class='fa fa-user'></i></div>
			<div class='large-11 columns'><input id='adminLoginUsernameInput' class='expand' type='text' placeholder='username...' /></div>
		</div>
		<div id='adminLoginPassword' class='row'>
			<div class='large-1 columns'><i class='fa fa-key'></i></div>
			<div class='large-11 columns'><input id='adminLoginPasswordInput' class='expand' type='password' placeholder='password...' /></div>
		</div>
		<div class='button expand' id='adminLoginSubmit'>Log In!</div>
	</div>
</div>

<div id='header' class='row fullWidth' style='position:absolute;z-index:1;top:0;left:0;background:#87A65C; width:100%;height:5rem;padding:.5rem;'>
	<div class='large-2 left-centered columns' style='margin:0 auto;'><img style='height:4rem;' src='/library/site/img/logo_w.png' /></div>
	<div class='large-8 left-centered columns'> </div>
	<div id='staffAccount' class='large-2 large columns valign' style='text-align:right;cursor:pointer;'>
		<div style='color:#FFF;padding-top:.5rem;'>Welcome back <span class='staffFirstName'></span></div>
	</div>
</div>